---
layout: page
title: About
permalink: /about/
---

# Hi! I am Aayush.

I'm an **Open Source Enthusiast** from Bhilai, India. I work on Open Source Softwares while learning new things everyday.

I created this website in order to provide simple and well documented guides on various projects that I worked on. In case you enjoy my work, please consider supporting me through [PayPal](https://www.paypal.me/aayushgupta).
