---
layout: post
title:  "What is Git Subtree Strategy and How to Use it"
date:   2018-12-31 20:16:00 +0530
categories: git
---

**This article aims to explain the need and use of git subdir strategy during work with different projects in context with Android**

While working on a project I encountered a strange situation once and that was that I had to import another project inside of my current project. The situation was strange because I had to keep another project that I was going to import updated or will have to merge the latest updates from time to time.

#### Example Situation

_A nice explanation of the situation would be something like this_

- **My current project:** An Android Kernel
- **Target project to import:** QCOM's WLAN module
- **Aim:** To ship a standalone kernel with a different OS with a working wifi

I am required not only to import the current project but also would be needed to do device specific changes to the WLAN module and enable its compilation with my Kernel.

#### **Note:**

In case, if I just want to import or track another project without doing any changes, I could have used `git submodule` feature which lets you import another repo/project in your current project without any additional work.

However, it's important to note that `git submodule` will let me track that another project into my current repo but it the disadvantage will be that I would have to merge my changes in target repo which I am going to track and as the changes are device specific, it makes even less sense to merge my changes into a project which is generic rather than device specific.

I can obviously fork my copy of target project and track it, but lets for the sake of this article we don't assume that option here.

#### **Enter git subtree stratergy**

Git-subtree strategy as the name suggests, allows you to import another project into a specific directory as well as lets you merge, cherry-pick and do all the cool stuff that you would normally do in the root of your project.

#### **How to use git subtree strategy?**

I will use an example situation to explain it. Proper commands regarding what you want to do with subtree strategy can be obtained using `man git` or the web.

**Example Situation to import target project and update if it doesn't exist in subdir yet**

- **My Current Project:** An Android kernel
- **Target Project to Import:** prima repo of CodeAurora
- **Aim:** Import prima repo to a subdir of current kernel project to do tinker around

Add the target project as remote and fetch the project.

`git remote add caf https://source.codeaurora.org/quic/la/platform/vendor/qcom-opensource/wlan/prima`

`git fetch caf`

Create a branch temp and set it to track the desired branch of your target project. This will make it easier to merge updates in the future.

`git checkout -b temp caf/LA.BR.1.2.9.1_rb1.5`

Switch back to the default branch

`git checkout master`

Import temp branch's files into your desired subdir

`git read-tree --prefix=drivers/staging/prima -u temp`

Commit the changes

`git commit`

Later to merge new changes, just checkout branches, pull latest changes from target remote and merge using `-s subtree` option.

`git checkout temp`

`git pull`

`git merge -s subtree`

You are free to squash or do whatever you want.

**Example Situation to update target project if it already exists in subdir**

- **My Current Project:** An Android kernel
- **Target Project to Import Updates From** prima repo of CodeAurora
- **Aim:** Import changes done in a new tag in prima repo by CodeAurora

Now as per me, the best way to get those new changes from upstream would be either by `cherry-pick` or `merge`.

Our project is in path `$SOURCE` and target project is in path `$SOURCE/drivers/staging/prima`.

Fetch the target project to get changes from

`git fetch https://source.codeaurora.org/quic/la/platform/vendor/qcom-opensource/wlan/prima LA.BR.1.2.9.1-02310-8x16.0`

Cherry pick the change/s to subdir using sha of the commit/s

`git cherry-pick -x --strategy=subtree -Xsubtree=drivers/staging/prima/ fa90f580a6384f29713b9b24963c10c346a2eda7`

**or**

`git cherry-pick -X subtree=drivers/staging/prima/ fa90f580a6384f29713b9b24963c10c346a2eda7..d27773cc6d3b5d3281f5b02fd93c1ab61ae876c1`

**or**

`git merge -X subtree=drivers/staging/prima FETCH_HEAD`

If required, pass `-n` or use `rebase` to squash commits later.

You can also do all the cool stuff like fetching directly branches-tags rather than adding as remote, rebasing, squashing etc. You can take a look at documentation by using `man git` or search the web, ask on forums, read books and all.


#### Credits:

- [git-scm](https://git-scm.com/)
- [StackOverflow](https://stackoverflow.com/)
- [The GitHub Blog](https://blog.github.com/)
