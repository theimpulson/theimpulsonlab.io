---
layout: post
title:  "How to Check Current CAF Tag of an Android Kernel"
date:   2018-12-21 13:12:00 +0530
categories: android kernel
---

**This article explains how to check the current CAF tag of an android kernel**

I have seen a lot of peoples asking on many forums on how to check the current CAF Tag of an Android kernel, so, I decided to write about it.

A lot of OEM are nowadays releasing Android Kernel Source in the compressed format and they also avoid including git log which makes it more difficult to track changes. However, restoring and creating a sensible git log is a nice thing to discuss but let's save that for another time.

The current goal is on how to find out the current CAF tag of an Android Kernel.

## Method 1

We will using your device's firmware/modem for this. In case your device is online, just run a `grep` into firmware for CAF Tag. An example will be `grep -rn "sdm660"`. You can expect the result to the CAF Tag of your current stock kernel. In case you have modified kernel, modem or anything else in your ROM, this will obviously indicate different results.

### A Dummy Example

`cd` into firmware directory

`grep -rn "LA.UM"`

RESULT will be something like this:  `verinfo/ver_info.txt:4:        "apps": "LA.UM.6.2.r1-06900-sdm660.0-1",`

## Method 2

We will be using dotfiles by  **Luca Stefani(luca020400)**.

### URLs:

GitHub Repo: [dotfiles](https://github.com/luca020400/dotfiles)

CAF Kernel Sources: [codeaurora](https://source.codeaurora.org/quic/la/kernel/)

CodeAurora Release Page: [CodeAurora's Release Page](https://wiki.codeaurora.org/xwiki/bin/QAEP/release)

We will be using `best_kernel` for this purpose, whose aim is to compare your current kernel source code with different CAF tags and select the best possible match using `git diff`.

### How To:

Clone and setup dotfiles as mentioned in README of the repo.

Clone/Download your kernel repo and commit your changes(if not already committed) as `best_kernel` depends upon `git diff` to compare changes.

Depending upon your kernel version, add required CAF kernel repo as remote and fetch it.
 `best_kernel` will use this fetched data to compare changes.

After fetch is done, run `best_kernel`. It should run automatically if you have placed dotfiles in `$BIN` directory and have necessary dependencies installed.

`best_kernel` will compare changes and will provide results based on that. However, depending on the number of tags, it will take time. A lot of time, so go and grab a cup of coffee till then.

### A Dummy Example:

Downloading stock kernel source code from OEM's website

`wget http://nokiaphones-opensource.azureedge.net/download/phones/Nokia6.1_V2.22J.tar.bz2 && tar xvjf Nokia6.1_V2.22J.tar.bz2 && cd kernel`

Adding and committing all files due to lack of a `.git` dir.

`git init && git add --all && git commit -m "Initial Commit"`

As the kernel source code version is 4.4, I will add 4.4 remote from CAF and fetch it.

`git remote add caf https://source.codeaurora.org/quic/la/kernel/msm-4.4/ && git fetch caf`

After fetch, I will check for current CAF tag using `best_kernel`. As my device platform is `sdm660` and CAF has been releasing `sdm660` tags as `sdm660.0`, I will it as an argument in `best_kernel`. This will ensure that `best_kernel` doesn't waste time checking CAF Tags of other platforms and will speed up the process considerably.

`best_kernel "*sdm660.0"`

RESULT will be something like this:

`Best match
TAG: LA.UM.6.2.r1-06900-sdm660.0
Lines changed: 177924`

### Hints:

- You can pass additional arguments to `best_kernel` to speed up the process.

  for e.g. `best_kernel "LA.BR.1.2*"` or `best_kernel "*8x16.0"` or depending upon your usage.

- It might be a better idea to pull firmware on PC and then search or tweak.

### Credits:

- [luca020400](https://github.com/luca020400)
- Everyone who helped me
